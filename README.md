The Arduino String Builder is a small utility to quickly create a simple string containing all the words needed for the Arduino to output. The individual words can be retrieved via a defined coding. The software does the coding and assembles the string from the table. All words are stored in an array and can be used globally in the program.

![grafik.png](./grafik.png)
